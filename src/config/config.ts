import * as dotenv from 'dotenv';

dotenv.config({ path: `${__dirname}/.env` });

const ENV = process.env;

export default Object.freeze({
  PORT: Number(ENV.port) || 4040,
  JWT_SECRET: ENV.JWT_SECRET || 'test',
  JWT_EXPIRE: ENV.JWT_EXPIRE || 604800000,

  MONGODB_URL: ENV.MOGNODB_URL || 'mongodb://localhost:27017',
  MOGNODB_NAME: ENV.MOGNODB_NAME || 'test',
});
