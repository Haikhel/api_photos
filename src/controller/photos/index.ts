import axios from 'axios';
import express, { Router } from 'express';
import errors from '../../config/errors';
import authUser from '../../helper/user/authUser';
import { ILoadPhotoData } from '../../interfaces/controller/photo';
import { IAlbum } from '../../interfaces/models/album';
import { IPhoto } from '../../interfaces/models/photo';
import AlbumsSchema from '../../models/album';
import PhotosSchema from '../../models/photos';
import UserSchema from '../../models/user';

class PhotoController {
  public path:string = '/photo';

  public router:Router = express.Router();

  constructor() {
    this.intializeRoutes();
  }

  public intializeRoutes():void {
    this.router.post(`${this.path}/load-photos`, this.loadPhoto);
    this.router.get(`${this.path}/get-photos`, this.getPhotos);
    this.router.post(`${this.path}/delete-photos`, this.deletePhotos);
    this.router.post(`${this.path}/delete-albums`, this.deleteAlbums);
    this.router.put(`${this.path}/change-album-title`, this.changeAlbumTitle);
  }

  private async loadPhoto(request: express.Request, response: express.Response):Promise<void> {
    const user = await authUser(request);

    if (!user) {
      response.status(404).send({ message: errors.User.USER_NOT_FOUND });
      return;
    }

    try {
      const photoLink:string = 'http://jsonplaceholder.typicode.com/photos';

      const photos:ILoadPhotoData[] = await (await axios.get(photoLink)).data;

      const albums: IAlbum[] = [];

      photos.forEach((elem: ILoadPhotoData):void => {
        if (!(albums.find((el:IAlbum):boolean => el.title === elem.albumId.toString()))) {
          albums.push({
            title: elem.albumId.toString(),
            owner: user.id,
          });
        }
      });

      const loadAlbum = await AlbumsSchema.insertMany(
        albums,
      );

      const photosLoadInfo:IPhoto[] = photos.map((elem:ILoadPhotoData):IPhoto => {
        const { id, albumId, ...photoInfo } = elem;

        return {
          ...photoInfo,
          owner: user.id,
          albumId: (loadAlbum.find((el):boolean => el.title === albumId.toString()))?.id || '',
        };
      });

      await PhotosSchema.insertMany(photosLoadInfo);

      response.send({ status: 'Success' });
    } catch (error) {
      response.send({ message: errors.Photo.UNEXPECTED_ERROR });
    }
  }

  private async getPhotos(request: express.Request, response: express.Response):Promise<void> {
    const { ownerId, page, maxCount } = request.query;

    const user = await UserSchema.findOne({ _id: ownerId });

    if (!user) {
      response.send({ message: errors.User.USER_NOT_FOUND });
      return;
    }
    try {
      const photos = await PhotosSchema.find({
        owner: user?.id,
      }, {}, { skip: Number(page), limit: Number(maxCount) });

      response.send(photos);
    } catch (error) {
      response.send({ message: errors.Photo.UNEXPECTED_ERROR });
    }
  }

  private async deletePhotos(request: express.Request, response: express.Response):Promise<void> {
    const user = await authUser(request);

    if (!user) {
      response.status(404).send({ message: errors.User.USER_NOT_FOUND });
      return;
    }

    const { photoid }: { photoid:string } = request.body;

    try {
      await PhotosSchema.find({
        _id: { $in: photoid.split(',') },
      }).remove();

      response.send({ status: 'Success' });
    } catch (error) {
      response.send({ message: errors.Photo.UNEXPECTED_ERROR });
    }
  }

  private async deleteAlbums(request: express.Request, response: express.Response):Promise<void> {
    const user = await authUser(request);

    if (!user) {
      response.status(404).send({ message: errors.User.USER_NOT_FOUND });
      return;
    }

    const { albumsId }: { albumsId:string } = request.body;

    try {
      const arrayAlbumsId:string[] = albumsId.split(',');

      await AlbumsSchema.remove({
        _id: { $in: arrayAlbumsId },
      });

      await PhotosSchema.remove({
        albumId: { $in: arrayAlbumsId },
      });

      response.send({ status: 'Success' });
    } catch (error) {
      response.send({ message: errors.Photo.UNEXPECTED_ERROR });
    }
  }

  private async changeAlbumTitle(request: express.Request, response: express.Response):Promise<void> {
    const user = await authUser(request);

    if (!user) {
      response.status(404).send({ message: errors.User.USER_NOT_FOUND });
      return;
    }
    try {
      const { albumId, newAlbumName } = request.query;

      await AlbumsSchema.findOneAndUpdate({
        _id: albumId,
        title: newAlbumName,
      });

      response.send({ status: 'Success' });
    } catch (error) {
      response.status(404).send({ message: errors.User.USER_NOT_FOUND });
    }
  }
}

export default PhotoController;
