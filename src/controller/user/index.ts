import CryptoJS from 'crypto-js';
import express from 'express';
import UserSchema from '../../models/user';
import errors from '../../config/errors';
import { ILoginUser, IRegisterUser } from '../../interfaces/controller/user';
import authUser from '../../helper/user/authUser';
import { IUser } from '../../interfaces/models/user';

class UserController {
  public path = '/user';

  public router = express.Router();

  constructor() {
    this.intializeRoutes();
  }

  public intializeRoutes() {
    this.router.post(`${this.path}/singUp`, this.signUp);
    this.router.post(`${this.path}/signIn`, this.signIn);
    this.router.get(`${this.path}/current`, this.getCurrentUser);
  }

  private async signUp(request: express.Request, response: express.Response) {
    const { login, email, password }: IRegisterUser = request.body;

    const user = await UserSchema.findOne({ $or: [{ login }, { email }] });

    if (user) {
      response.status(404).send({ message: errors.User.ALREADY_EXISTS });
      return;
    }

    const hashedPassword = CryptoJS.MD5(password).toString();

    const userModel = new UserSchema({
      login,
      email,
      hashPassword: hashedPassword,
    });

    await userModel.save();

    response.status(200).send({
      token: userModel.signIn(),
      user: userModel,
    });
  }

  private async signIn(request: express.Request, response: express.Response):Promise<void> {
    const { login, password }: ILoginUser = request.body;

    const reg:RegExp = /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/;

    let user;

    if (reg.test(login)) {
      user = await UserSchema.findOne({ email: login });
    } else {
      user = await UserSchema.findOne({ login });
    }

    if (!user) {
      response.status(404).send({ message: errors.User.USER_NOT_FOUND });
      return;
    }

    if (user.hashPassword !== CryptoJS.MD5(password).toString()) {
      response.status(404).send({ message: errors.User.INCORRECT_PASSWORD });
    }

    response.status(200).send({
      token: user.signIn(),
      user,
    });
  }

  public async getCurrentUser(request: express.Request, response: express.Response):Promise<void> {
    const user:IUser = await authUser(request);
    if (!user) {
      response.status(404).send({ message: errors.User.USER_NOT_FOUND });
      return;
    }
    response.send(user);
  }
}

export default UserController;
