import express from 'express';
import jwt from 'jsonwebtoken';
import config from '../../config/config';
import UserSchema from '../../models/user';

export default async (req:express.Request):Promise<any> => {
  const bearerHeader:string | undefined = req.headers.authorization;

  if (!bearerHeader) {
    return null;
  }

  const token:string[] = bearerHeader.split(' ');

  if (!token || !token[1]) {
    return null;
  }

  try {
    const options = async (error: any, tokenData: any) => {
      if (error) throw new Error('token error');

      return UserSchema.findOne().where({
        authToken: tokenData.authKey,
      });
    };
    const user:void = jwt.verify(token[1], config.JWT_SECRET, options);
    return user;
  } catch (error) {
    return null;
  }
};
