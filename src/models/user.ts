import {
  Document, model, Schema,
} from 'mongoose';
import jwt from 'jsonwebtoken';
import config from '../config/config';
import { IUser } from '../interfaces/models/user';

interface UserBaseDocument extends IUser, Document {
  signIn(): string;
}
const UserSchema = new Schema<UserBaseDocument>({
  login: { type: String, required: true },
  email: { type: String, required: true },
  hashPassword: { type: String, required: true },
  registerDate: { type: Date },
  authToken: { type: String },
});

UserSchema.pre<IUser>('save', function (next) {
  const now = new Date();
  if (!this.registerDate) {
    this.registerDate = now;
  }
  next();
});

UserSchema.methods.signIn = function signIn():string {
  return jwt.sign(
    { authKey: this.authToken },
    config.JWT_SECRET,
    { expiresIn: config.JWT_EXPIRE },
  );
};

// UserSchema.methods.getUserByToken = async function getUserByToken(token: string) {
//   try {
//     const options = async (error: any, tokenData: any) => {
//       if (error) throw new Error('token error');
//       return this.findOne().where({
//         authToken: tokenData.authKey,
//       });
//     };
//     return jwt.verify(token, config.JWT_SECRET, options);
//   } catch (error) {
//     return null;
//   }
// };

export default model<UserBaseDocument>('User', UserSchema);
