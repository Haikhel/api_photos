import mongoose, { Schema } from 'mongoose';
import { IAlbum } from '../interfaces/models/album';

const AlbumsSchema = new Schema<IAlbum>({
  title: { type: String, required: true },
  owner: { type: Schema.Types.ObjectId, ref: 'users' },
});

export default mongoose.model('albums', AlbumsSchema);
