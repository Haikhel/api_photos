import mongoose, { Schema } from 'mongoose';
import { IPhoto } from '../interfaces/models/photo';

const PhotosSchema = new Schema<IPhoto>({
  title: { type: String, required: true },
  url: { type: String, required: true },
  thumbnailUrl: { type: String, required: true },
  owner: { type: Schema.Types.ObjectId, ref: 'users' },
  albumId: { type: Schema.Types.ObjectId, ref: 'albums' },
});

export default mongoose.model('photos', PhotosSchema);
