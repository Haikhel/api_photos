import { Types } from 'mongoose';

export interface IPhoto{
  title: string
  url: string
  thumbnailUrl: string
  owner: Types.ObjectId
  albumId: Types.ObjectId
}
