export interface IUser {
  login: string
  email: string
  hashPassword: string
  registerDate?: Date
  authToken?:string
}
