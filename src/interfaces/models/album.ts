import { Types } from 'mongoose';

export interface IAlbum{
  title: string
  owner: Types.ObjectId
}
