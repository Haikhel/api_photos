import { IUser } from '../models/user';

export interface IUserResponse{
  user: IUser
  token: string
}

export interface ILoginUser{
  login:string;
  password:string;
}

export interface IRegisterUser{
  login:string;
  email:string;
  password:string;
}
