export interface ILoadPhotoData{
  albumId: number
  id: number
  title: string
  url:string
  thumbnailUrl: string
}
