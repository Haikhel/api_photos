import App from './app';
import UserController from './controller/user/index';
import config from './config/config';
import './db/connect';
import PhotoController from './controller/photos';

const app:App = new App(
  [
    new UserController(),
    new PhotoController(),
  ],
  config.PORT,
);
app.listen();
